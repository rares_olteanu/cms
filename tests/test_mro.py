class A(object):
    def __init__(self, a, b):
        self.a = a
        self.b = b
    def __repr__(self):
        return repr(self.__dict__)
    def __new__(cls, *args, **kwargs):
        print(args, kwargs)
        obj = object.__new__(cls)
        print(obj)
        return obj


a=A(1, 2)

def func(*args):
    print(args)


































class A(object):
    def foo(self):
        print('A', end='')

class B(A):
    def foo(self):
        print('B', end='')
        super(B, self).foo()

class C(A):
    def foo(self):
        print('C', end='')
        super(C, self).foo()

class D(B, C):
    def foo(self):
        print('D', end='')
        super(D, self).foo()

D().foo()

class C(A, B):
    pass
