from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.db import transaction
from django.http import HttpResponseBadRequest
from django.shortcuts import reverse, redirect, render, get_object_or_404
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.views.generic.detail import BaseDetailView, SingleObjectTemplateResponseMixin
from django.views.generic.edit import FormMixin, DeletionMixin

from .forms import PageForm, PageDeleteForm, SidebarPageForm, SidebarItemFormset
from .models import Page, Tag, SidebarPage, SidebarItem


class IndexView(ListView):
    queryset = Page.objects.filter(is_draft=False)
    paginate_by = 10
    ordering = 'pk'

    def get_context_data(self):
        return super(IndexView, self).get_context_data(
            tags=Tag.objects.filter(page__is_draft=False).values('name').distinct(),
            moderate_count=Page.objects.filter(is_draft=True).count()
            if self.request.user.has_perm('cmsapp.can_publish') else 0,
        )

    def get_queryset(self):
        qs = super(IndexView, self).get_queryset()
        if 'tag' in self.kwargs:
            return qs.filter(tag__name=self.kwargs['tag'])
        else:
            return qs


class ModerateIndexView(ListView):
    queryset = Page.objects.filter(is_draft=True)
    paginate_by = 10
    ordering = 'pk'
    template_name_suffix = '_moderate_list'


@permission_required('cmsapp.can_publish')
@transaction.atomic
def page_moderate(request, pk):
    basedraft = get_object_or_404(Page, is_draft=True, pk=pk)
    draft = getattr(basedraft, 'sidebarpage', basedraft)

    if request.method == "POST":
        action = request.POST.get('action')
        if action == 'publish':
            old_page = draft.revision_of
            if old_page:
                draft.revision_of = None
                draft.save()
                old_page.delete()
            draft.is_draft = False
            draft.save()
            messages.add_message(
                request, messages.WARNING,
                _("Draft for {title!r} was published.").format(title=draft.title))
        elif action == 'discard':
            draft.delete()
            messages.add_message(
                request, messages.WARNING,
                _("Draft for {title!r} was discarded.").format(title=draft.title))
        else:
            return HttpResponseBadRequest("Looks like someone is messing around ...")
        return redirect("cmsapp:moderate")
    return render(request, "cmsapp/page_moderate_detail.html", {
        'page': draft,
        'sidebar': getattr(basedraft, 'sidebarpage', None),
        'tags': draft.tags.all(),
    })


class PageView(DetailView):
    queryset = Page.objects.filter(is_draft=False)
    slug_url_kwarg = 'slug'  # this is by default

    def get_context_data(self, object):
        return super(PageView, self).get_context_data(
            page=object,
            sidebar=getattr(object, 'sidebarpage', None),
            tags=object.tags.all()
        )


@method_decorator(login_required, name='dispatch')
class PageDeleteView(DeletionMixin, FormMixin, SingleObjectTemplateResponseMixin, BaseDetailView):
    template_name_suffix = '_confirm_delete'
    model = Page
    form_class = PageDeleteForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.delete(request)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('cmsapp:index')


@method_decorator(login_required, name='dispatch')
class PageAddView(CreateView):
    model = Page
    form_class = PageForm


@login_required
@transaction.atomic
def page_change(request, slug=None, kind='page'):
    if slug is None:
        instance = None
    else:
        instance = get_object_or_404(Page, slug=slug, is_draft=False)
        if getattr(instance, 'sidebarpage', None):
            kind = 'sidebar'
            instance = instance.sidebarpage
    if kind == 'page':
        Model = Page
        Form = PageForm
        Formset = None
    elif kind == 'sidebar':
        Model = SidebarPage
        Form = SidebarPageForm
        Formset = SidebarItemFormset
    else:
        raise RuntimeError("Unexpected kind %r!" % kind)

    pending_draft = getattr(instance, 'pending_draft', None)
    if pending_draft:
        return redirect("cmsapp:moderate", pk=pending_draft.pk)

    if request.method == "POST":
        draft = Model(is_draft=True, revision_of=instance)
        form = Form(request.POST, instance=draft)
        if Formset:
            # inlinemodelformset would need the „instance” argument
            formset = Formset(request.POST)  # , instance=instance)
        else:
            formset = None
        if form.is_valid() and (not formset or formset.is_valid()):
            form.save()
            if formset:
                for data in formset.cleaned_data:
                    if data:
                        SidebarItem.objects.create(sidebar_page=draft, **data)
            messages.add_message(
                request, messages.INFO,
                _("Your page {title!r} was saved and it's waiting moderation. A moderator can publish it.").format(
                    title=draft.title))
            return redirect("cmsapp:index")
    else:
        form = Form(instance=instance)
        # inlinemodelformset would need the „instance” argument
        formset = Formset and Formset(initial=instance.sidebar_items.values())  # instance=instance)

    return render(request, "cmsapp/page_form.html", {
        'form': form,
        'formset': formset,
        'title': (
            _('Editing {model}') if instance else _('Adding a {model}')
        ).format(model=Model._meta.verbose_name),
    })


@method_decorator(login_required, name='dispatch')
class PageChangeView(UpdateView):
    model = Page
    form_class = PageForm


def bad(request):
    raise Exception("Boooo %s!" % Page.objects.count())  # pragma: nocover
