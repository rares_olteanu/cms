import os

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CmsappConfig(AppConfig):
    name = 'cmsapp'
    # label = 'cms'  # this will break your migrations
    verbose_name = _('CMS')  # customize display name in admin

    def ready(self):
        print("======== Django is ready! (pid:%s) ========" % os.getpid())

