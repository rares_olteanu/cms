#!/usr/bin/env python
import os
import sys
import aspectlib
from aspectlib import debug
# import logging
# logging.basicConfig(level="DEBUG")
# aspectlib.weave_module("django.utils.translation.trans_real", debug.log, "activate")

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cmsproject.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    # import hunter
    # with hunter.trace(
    #         function="activate",
    #         module_sw="django.utils.translation",
    #         kind_in='call,exception,return',
    #         actions=[hunter.CallPrinter, lambda _: print('    ', debug.format_stack(skip=1, length=10))]
    # ):
    execute_from_command_line(sys.argv)
