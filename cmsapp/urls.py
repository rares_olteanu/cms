from django.conf.urls import url

from . import views

app_name = 'cmsapp'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^BAD/$', views.bad),
    url(r'^add/page/$', views.page_change, name='add'),
    url(r'^add/sidebarpage/$', views.page_change, {'kind': 'sidebar'}, name='add-sidebar'),
    url(r'^tag/(?P<tag>[^/]+)/$', views.IndexView.as_view(), name='index'),
    url(r'^moderate/$', views.ModerateIndexView.as_view(), name='moderate'),
    url(r'^moderate/(?P<pk>[0-9]+)/$', views.page_moderate, name='moderate'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', views.PageView.as_view(), name='page'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/edit$', views.page_change, name='change'),
    # url(r'^(?P<slug>[-a-zA-Z0-9_]+)/edit$', views.PageChangeView.as_view(), name='change'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/delete$', views.PageDeleteView.as_view(), name='delete'),
]
