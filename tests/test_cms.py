import pytest
from django.http import HttpResponse
from django.test import Client

from cmsapp.forms import PageForm
from cmsapp.models import Page, Tag


@pytest.fixture
def client(request, client):
    func = client.request

    def wrapper(**kwargs):
        print('>>>>', ' '.join('{}={!r}'.format(*item) for item in kwargs.items()))
        resp = func(**kwargs)
        print('<<<<', resp, resp.content)
        return resp

    client.request = wrapper
    return client


@pytest.fixture
def authed_client(db, client, admin_user):
    client.force_login(admin_user)
    return client


def test_pageadmin(pages, client: Client, admin_user):
    client.force_login(admin_user)
    resp = client.get('/admin/cmsapp/page/')
    assert resp.status_code == 200


def test_page_repr():
    assert repr(Page(slug=1, title=2, content=3)) == '<Page #None draft=False slug=1 title=2 content=3>'


def test_tag_repr():
    assert repr(Tag(name=1, page=Page(slug=''))) == "<Tag page=None/'' name=1>"


def test_index(pages, client):
    """
    :type client: Client
    """
    resp = client.get('/')  # type: HttpResponse
    assert resp.status_code == 200

    assert list(resp.context['object_list']) == pages
    content = resp.content.decode(resp.charset)
    for page in pages:
        assert page.title in content


@pytest.mark.parametrize('url', ['/', '/tag/foobar/'])
def test_index_empty(db, client, url):
    resp: HttpResponse = client.get(url)
    assert resp.status_code == 200

    assert len(resp.context['object_list']) == 0
    content = resp.content.decode(resp.charset)
    assert 'No pages' in content


def test_show(pages, client):
    resp = client.get('/abc/')
    assert resp.status_code == 200
    assert resp.context['page'] == pages[0]
    content = resp.content.decode(resp.charset)
    assert 'lorem ipsum' in content


def test_show_redirect(pages, client):
    resp: HttpResponse = client.get('/abc')
    assert resp.status_code == 301
    assert resp.url == '/abc/'


@pytest.mark.parametrize('tags', ['t1,t 2', 't1,t 2,', 't1,,,,t 2', ','])
def test_add(db, authed_client, tags):
    resp = authed_client.post('/add/page/', {
        'title': 'Foo',
        'slug': 'foo',
        'content': 'Baaaar',
        'tags': tags
    })
    content = resp.content.decode(resp.charset)
    print(content)
    assert resp.status_code == 302
    assert resp.url == '/'
    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'waiting moderation' in content

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'Foo' in content

    resp = authed_client.get('/moderate/%s/' % Page.objects.get().pk)
    content = resp.content.decode(resp.charset)
    assert 'Baaaar' in content
    if tags != ',':
        assert 'href="/tag/t1/"' in content
        assert 'href="/tag/t%202/"' in content

    resp = authed_client.get('/')
    content = resp.content.decode(resp.charset)
    assert 'No pages' in content

    resp = authed_client.post('/moderate/%s/' % Page.objects.get().pk, {'action': 'publish'})
    assert resp.status_code == 302

    resp = authed_client.get('/foo/')
    content = resp.content.decode(resp.charset)
    assert 'Baaaar' in content
    if tags != ',':
        assert 'href="/tag/t1/"' in content
        assert 'href="/tag/t%202/"' in content



def test_add_dupe(pages, authed_client):
    resp = authed_client.post('/add/page/', {'title': 'Foo', 'slug': 'foo', 'content': 'Baaaar'})
    content = resp.content.decode(resp.charset)
    print(content)
    assert resp.status_code == 200
    assert 'Page with this Slug already exists' in content


@pytest.mark.parametrize('data', [
    {'title': '', 'slug': 'bar', 'content': 'lorem ipsum'},
    {'title': 'foo', 'slug': 'bar', '': 'lorem ipsum'},
    {'title': 'foo', 'slug': 'bar', 'content': ''},
])
def test_add_empty(db, authed_client, data):
    resp = authed_client.post('/add/page/', data)
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'This field is required' in content


def test_edit(pages, authed_client):
    resp = authed_client.get('/foo/edit')
    assert resp.status_code == 200
    data = resp.context['form'].initial
    data['title'] = 'New Title'
    data['content'] = 'New Content'

    resp = authed_client.post('/foo/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/'

    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'waiting moderation' in content
    assert 'New Title' in content

    draft1 = Page.objects.get(is_draft=True)
    draft_pk1 = draft1.pk
    assert draft1.revision_of == pages[2]

    data['title'] = 'New Title2'
    data['tags'] = 'm1,m2'
    resp = authed_client.get('/foo/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/moderate/%s/' % draft_pk1

    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'Publish' in content
    assert 'Discard' in content
    assert 'New Content' in content

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'New Title' in content

    resp = authed_client.post('/moderate/%s/' % draft_pk1, {'action': 'publish'})
    assert resp.status_code == 302
    assert resp.url == '/moderate/'

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'Nothing to moderate left' in content


def test_edit_sidebar(sidebarpage, authed_client):
    resp = authed_client.get('/abc/edit')
    assert resp.status_code == 200
    data = {
        'slug': 'abcd',
        'title': 'NEWTITLE',
        'tags': 'sb1,sb2',
        'content': 'LUREOM',
        'sidebar_title': 'NEWSIDEBAR',
        'form-TOTAL_FORMS': '5',
        'form-INITIAL_FORMS': '0',
        'form-MIN_NUM_FORMS': '0',
        'form-MAX_NUM_FORMS': '1000',
        'form-0-content': 'item1',
        'form-1-content': 'item2',
    }
    resp = authed_client.post('/abc/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/'

    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'waiting moderation' in content
    assert 'NEWTITLE' in content

    assert len(Page.objects.all()) == 2
    draft1 = Page.objects.get(is_draft=True)
    draft_pk1 = draft1.pk
    assert draft1.revision_of.sidebarpage == sidebarpage

    data['title'] = 'New Title2'
    data['tags'] = 'sb1,sb2'
    resp = authed_client.post('/abc/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/moderate/%s/' % draft_pk1

    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'Publish' in content
    assert 'Discard' in content
    assert 'NEWTITLE' in content
    assert 'NEWSIDEBAR' in content
    assert 'href="/tag/sb1/"' in content
    assert 'href="/tag/sb2/"' in content

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'NEWTITLE' in content

    resp = authed_client.post('/moderate/%s/' % draft_pk1, {'action': 'publish'})
    assert resp.status_code == 302
    assert resp.url == '/moderate/'

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'Nothing to moderate left' in content

    resp = authed_client.get('/abcd/')
    content = resp.content.decode(resp.charset)
    assert 'NEWTITLE' in content
    assert 'NEWSIDEBAR' in content
    assert 'href="/tag/sb1/"' in content
    assert 'href="/tag/sb2/"' in content


def test_edit_sidebar_discard(sidebarpage, authed_client):
    resp = authed_client.get('/abc/edit')
    assert resp.status_code == 200
    data = {
        'slug': 'abcd',
        'title': 'NEWTITLE',
        'tags': 'sb1,sb2',
        'content': 'LUREOM',
        'sidebar_title': 'NEWSIDEBAR',
        'form-TOTAL_FORMS': '5',
        'form-INITIAL_FORMS': '0',
        'form-MIN_NUM_FORMS': '0',
        'form-MAX_NUM_FORMS': '1000',
        'form-0-content': 'item1',
        'form-1-content': 'item2',
    }
    resp = authed_client.post('/abc/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/'

    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'waiting moderation' in content
    assert 'NEWTITLE' in content

    assert len(Page.objects.all()) == 2
    draft1 = Page.objects.get(is_draft=True)
    draft_pk1 = draft1.pk
    assert draft1.revision_of.sidebarpage == sidebarpage

    data['title'] = 'New Title2'
    data['tags'] = 'sb1,sb2'
    resp = authed_client.post('/abc/edit', data)
    assert resp.status_code == 302
    assert resp.url == '/moderate/%s/' % draft_pk1

    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'Publish' in content
    assert 'Discard' in content
    assert 'NEWTITLE' in content
    assert 'NEWSIDEBAR' in content
    assert 'href="/tag/sb1/"' in content
    assert 'href="/tag/sb2/"' in content

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'NEWTITLE' in content

    resp = authed_client.post('/moderate/%s/' % draft_pk1, {'action': ''})
    assert resp.status_code == 400
    resp = authed_client.post('/moderate/%s/' % draft_pk1, {'action': 'discard'})
    assert resp.status_code == 302
    assert resp.url == '/moderate/'

    resp = authed_client.get('/moderate/')
    content = resp.content.decode(resp.charset)
    assert 'Nothing to moderate left' in content

    resp = authed_client.get('/abc/')
    content = resp.content.decode(resp.charset)
    assert 'NEWTITLE' not in content
    assert 'NEWSIDEBAR' not in content
    assert 'href="/tag/sb1/"' not in content
    assert 'href="/tag/sb2/"' not in content

def test_delete(pages, authed_client):
    resp = authed_client.get('/foo/delete')
    assert resp.status_code == 200
    resp = authed_client.post('/foo/delete')
    assert resp.status_code == 200
    content = resp.content.decode(resp.charset)
    assert 'Must confirm the delete' in content
    resp = authed_client.post('/foo/delete', {'delete': 'on'})
    assert resp.status_code == 302
    assert resp.url == '/'
    assert list(Page.objects.all()) == pages[:2]


def test_form(db):
    form = PageForm({'title': 'Foo', 'slug': 'foo', 'content': 'Baaaar'})
    assert form.is_valid()
    form.save(commit=False)
