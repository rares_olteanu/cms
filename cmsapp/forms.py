from django import forms
from djangoformsetjs.utils import formset_media_js

from .models import Page, Tag, SidebarPage, SidebarItem


class TagField(forms.CharField):
    def clean(self, value):
        value = super(TagField, self).clean(value)

        tags = [item.strip() for item in value.split(',')]
        return [tag for tag in tags if tag]


# an altenative to having multiple classes for
# each page specialization

# def pageform_factory(kind):
#     class PageForm(forms.ModelForm):
#         class Meta:
#             if kind == 'page':
#                 model = Page
#                 fields = "slug", "title", "tags", "content"
#             elif kind == 'sidebar':
#                 model = SidebarPage
#                 fields = "slug", "title", "tags", "content", "sidebar_title"
#             else:
#                 raise RuntimeError("Unexpected page kind %r!" % kind)
#
#         tags = TagField(required=False)
#
#         def __init__(self, data=None, *, initial=None, instance=None, **kwargs):
#             if instance:
#                 initial = initial or {}
#                 initial['tags'] = ', '.join(instance.tags.values_list('name', flat=True))
#             return super(PageForm, self).__init__(data, initial=initial, instance=instance, **kwargs)
#
#         def save(self, commit=True):
#             instance = super(PageForm, self).save(commit)
#             if commit:
#                 self.instance.tags.all().delete()
#                 Tag.objects.bulk_create([
#                     Tag(name=tag, page=instance)
#                     for tag in self.cleaned_data['tags']
#                 ])
#             return instance
#
#     return PageForm

# the advantage is that we could use the
# factory directly from the view
#   Form = pageform_factory('page')
#   Form = pageform_factory('sidebar')
# however not worth the hassle for just
# two specializations

class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = "slug", "title", "tags", "content"

    tags = TagField(required=False)

    def __init__(self, data=None, *, initial=None, instance=None, **kwargs):
        if instance:
            initial = initial or {}
            initial['tags'] = ', '.join(instance.tags.values_list('name', flat=True))
        return super(PageForm, self).__init__(data, initial=initial, instance=instance, **kwargs)

    def save(self, commit=True):
        instance = super(PageForm, self).save(commit)
        if commit:
            self.instance.tags.all().delete()
            Tag.objects.bulk_create([
                Tag(name=tag, page=instance)
                for tag in self.cleaned_data['tags']
            ])
        return instance

class SidebarPageForm(PageForm):
    class Meta:
        model = SidebarPage
        fields = "slug", "title", "tags", "content", "sidebar_title"



class SidebarItemForm(forms.Form):
    class Media:
        js = formset_media_js

    content = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}))


SidebarItemFormset = forms.formset_factory(form=SidebarItemForm, extra=5)

# we can't usa the inlineformset because it
# would move the items on the draft instead
# of copying them

# SidebarItemFormset = forms.inlineformset_factory(
#     SidebarPage, SidebarItem, fk_name='sidebar_page',
#     form=SidebarItemForm,
#     fields=["content"],
#     widgets={'content': forms.Textarea(attrs={'rows': 2})},
#     extra=5
# )


class PageDeleteForm(forms.Form):
    delete = forms.BooleanField(label="Yes, I'm sure I want to delete", required=False)

    def clean(self):
        cleaned_data = super(PageDeleteForm, self).clean()
        if not cleaned_data.get('delete'):
            raise forms.ValidationError('Must confirm the delete!')
        return cleaned_data
