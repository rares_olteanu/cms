import pytest

from cmsapp.models import Page, SidebarPage, SidebarItem


@pytest.fixture
def pages(request, db):
    return [
        Page.objects.create(slug='abc', title='ABC', content='lorem ipsum'),
        Page.objects.create(slug='123', title='1 2 3', content='lorem ipsum'),
        Page.objects.create(slug='foo', title='Bar', content='lorem ipsum'),
    ]


@pytest.fixture
def sidebarpage(request, db):
    page = SidebarPage.objects.create(slug='abc', title='ABC', content='lorem ipsum', sidebar_title="SIDEBAR TITLE")
    SidebarItem.objects.create(content='SIDEBAR LOREM IPSUM', sidebar_page=page)
    return page
