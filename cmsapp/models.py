from binascii import hexlify

from django.core.exceptions import ValidationError
from os import urandom

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class Tag(models.Model):
    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    name = models.CharField(verbose_name=_('Name'), max_length=200)
    page = models.ForeignKey("Page", verbose_name=_('Page'),
                             on_delete=models.CASCADE, related_name='tags', related_query_name='tag')

    def __repr__(self):
        return "<Tag page={0.page.pk}/{0.page.slug!r} name={0.name!r:.5}>".format(self)


def get_random_slug():
    return hexlify(urandom(8))


# class PageManager(models.Manager):
#     def get_queryset(self):
#         return super(PageManager, self).get_queryset().filter(is_draft=False, is_deleted=False)

class Page(models.Model):
    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')
        permissions = (
            ('publish', 'Can publish changes'),
        )
        unique_together = 'slug', 'is_draft'

    slug = models.SlugField(verbose_name=_('Slug'), max_length=200, blank=False, default=get_random_slug)
    title = models.CharField(verbose_name=_('Title'), max_length=200)
    content = models.TextField(verbose_name=_('Content'))

    is_deleted = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=False)
    revision_of = models.OneToOneField("self", null=True, related_name="pending_draft", on_delete=models.CASCADE, blank=True)

    def get_absolute_url(self):
        return reverse('cmsapp:page', kwargs={'slug': self.slug})

    def validate_unique(self, exclude=None):
        if self.is_draft:
            cls = type(self)
            qs = cls.objects.filter(slug=self.slug, is_draft=False)
            if self.pk:
                qs = qs.exclude(pk=self.pk)
            if self.revision_of:
                qs = qs.exclude(pk=self.revision_of_id)
            if qs.exists():
                raise ValidationError(self.unique_error_message(cls, ['slug']))
        super(Page, self).validate_unique(exclude)

    # @property
    # def moderation_id(self):
    #     return getattr(self, 'page_ptr_id', self.pk)

    def __repr__(self):  # https://pyformat.info/
        return "<{0.__class__.__name__} #{0.pk} draft={1} " \
               "slug={0.slug!r} title={0.title!r} content={0.content!r:.5}>".format(
            self,
            self.is_draft and self.revision_of_id
        )


class SidebarPage(Page):
    class Meta:
        verbose_name = _('SidebarPage')
        verbose_name_plural = _('SidebarPages')

    sidebar_title = models.CharField(max_length=200)


class SidebarItem(models.Model):
    sidebar_page = models.ForeignKey("SidebarPage",
                                     on_delete=models.CASCADE,
                                     related_name='sidebar_items',
                                     related_query_name='sidebar_item')
    content = models.TextField()
