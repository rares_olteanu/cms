from django.contrib import admin

from .models import Page, Tag, SidebarPage, SidebarItem


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = "name", "page_id"

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = "slug", "pk", "is_draft", "is_deleted", "revision_of_id", "title", "trimmed_content"

    def trimmed_content(self, obj):
        return obj.content[:10]

    trimmed_content.admin_order_field = "content"
    trimmed_content.short_description = "Content"

class SidebarItemInline(admin.TabularInline):
    model = SidebarItem

@admin.register(SidebarPage)
class SidebarPageAdmin(PageAdmin):
    inlines = SidebarItemInline,
